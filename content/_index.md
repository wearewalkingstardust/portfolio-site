---
title: "Home"
date: 2019-09-21T11:59:50-04:00
draft: false
sidebar: true
menu: "main"
--- 

<img src="/portfolio-site/images/kalib.jpg" alt="A headshot of Kalib" id="home-img" width="40%" style="border-radius: 100%;" />

Thanks for stopping by! My name is Kalib Watson, and I'm a technical writer, web developer, and accessibility advocate.  

If you'd like to see samples of my work, please visit the [Projects section](/projects/) of the site.

I am currently searching for technical writing work, and would love to hear from you! You can contact me at [k.watts999@gmail.com](mailto:k.watts999@gmail.com).  

Have a great day!