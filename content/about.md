---
title: "About"
date: 2019-09-21T12:08:02-04:00
draft: false
menu: "main"
---

As you may have already read, I'm a technical writer, web developer, and accessibility advocate. I've been working on the web for about 5 years now, and don't want that to change.

I believe the web is a fascinating place and an unprecendented opportunity to gain knowledge, relationships, and context. To me, the web is the 21st century library, only quieter and with better cross-reference abilities. My work has been focused on bettering the organization, accessibility, information, and relationships that exist within browsers, and this has led me from user experience to web development, and eventually to management. Now, I'm looking to get back to what I really love doing: Writing.  

Although I have been immersed in creating websites and making them accessible, nothing lights up my world like writing. Whether for creative purposes or to help others understand a difficult concept, I love taking a complex idea and making it shareable. I also enjoy being a teacher and helping others, and writing is my favorite way to do so.

I'm currently looking for ways to put my writing, web, and teaching experiences to work! If you've got a technical writing role that you think I'd be good for, please [reach out via email!](mailto:k.watts999@gmail.com)