---
title: "Resume"
date: 2019-09-21T12:07:57-04:00
draft: false
menu: "main"
---

## Skills
- HTML5
- CSS3
- JavaScript
- WordPress
- User Research
- Communication
- Docs-as-code

## Experience

### Web Accessibility Manager, ITHAKA
April, 2019 - present  
Ann Arbor, MI

- Creating processes and resources for developing accessible web experiences
- Creating and delivering training to QA, development, and UX staff members
- Collaborating with QA, developers, designers, and product owners
- Producing, publishing, and ongoing management of product VPAT documentation
- Creating and maintaining accessibility resources for product users
- Assisting agile teams in design and development of accessible solutions

### Web Developer I, Jackson
May 2017 - April, 2019  
Lansing, MI

- Creating processes and resources for developing accessible web experiences
- Creating and delivering training to QA, development, and UX staff members
- Developing and maintaining [Jackson's Charitable Foundation website](https://jacksoncharitablefoundation.org)
- Creating and maintaining documentation for the Charitable Foundation site
- Collaborating with QA, developers, designers, marketing, legal, and business owners

### Communications Intern, MECA
May 2016 - January 2017  
Lansing, MI

- Proofreading monthly "Country Lines" magazine content
- Managing communities for MECA's websites
- Writing and distributing three staff-focused newsletters per month
- Writing and editing articles for "Country Lines" magazine and newsletters
- Documenting WordPress processes for internal staff
- Collaborating with staff members across Michigan via email and phone

## Education

### B.A. in Professional Writing, Michigan State University
August 2014 - May 2018  
East Lansing, MI

The Professional Writing major allowed me to explore writing, editing, web development, and user experience while giving me great communication skills, the ability to take context into consideration, and most importantly, the lesson to practice empathy in all I do.